using System;

namespace Notegate
{
    public readonly struct Note
    {
        public string Title { get; }
        public string Content { get; }
        public DateTime Date { get; }

        public Note(string title, string content, DateTime? date = null)
        {
            Title = title;
            Content = content;
            Date = date ?? DateTime.Today;
        }
    }
}
